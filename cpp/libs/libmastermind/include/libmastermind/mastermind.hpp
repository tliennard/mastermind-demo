/* Mastermind demo.
Copyright (C) 2022  Thomas Liennard

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <vector>

namespace libmastermind {

struct GuessResult {
  int correctColor{0};
  int correctColorAndPosition{0};
};

class Code {
 public:
  Code(size_t num_pegs);

  Code(const Code& from);
  Code(Code&& from) noexcept;
  ~Code();

  Code& operator=(const Code& from);
  Code& operator=(Code&& from) noexcept;

  size_t numPegs() const noexcept;

  unsigned operator[](size_t index) const;
  unsigned& operator[](size_t index);

  //! returns true if value is present in code.
  bool hasValue(unsigned value) const noexcept;

  GuessResult checkGuess(const Code& guess) const;

 private:
  std::vector<unsigned> mPegs;
};

struct Guess {
  Code guess;
  GuessResult result;
};

class Game {
 public:
  Game();

  size_t numPegs() const noexcept;
  size_t maxGuesses() const noexcept;

  const Code& code() const noexcept;
  std::vector<Guess> guesses() const noexcept;

  bool isFinished() const noexcept;
  bool codeFound() const noexcept;

  void startGame(const Code& code);
  void tryGuess(const Code& guess);

 private:
  Code mCode;
  std::vector<Guess> mGuesses;
  bool mCodeFound{false};
};

}  // namespace libmastermind