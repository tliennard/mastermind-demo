#include "libmastermind/mastermind.hpp"

#include <gtest/gtest.h>

using namespace libmastermind;

TEST(CodeTest, CheckConstructor) {
  Code code1(4);
  ASSERT_EQ(code1.numPegs(), 4);
  ASSERT_EQ(code1[0], 0);
  ASSERT_EQ(code1[1], 0);
  ASSERT_EQ(code1[2], 0);
  ASSERT_EQ(code1[3], 0);

  Code code2{5};
  ASSERT_EQ(code2.numPegs(), 5);
}

TEST(CodeTest, CheckElementAcces) {
  Code code1(4);
  code1[0] = 3;
  code1[1] = 2;
  code1[2] = 4;
  code1[3] = 1;

  ASSERT_EQ(code1[0], 3);
  ASSERT_EQ(code1[1], 2);
  ASSERT_EQ(code1[2], 4);
  ASSERT_EQ(code1[3], 1);
}

TEST(CodeTest, CheckCopyConstructor) {
  Code code1(4);
  code1[0] = 3;
  code1[1] = 2;
  code1[2] = 4;
  code1[3] = 1;

  Code code2{code1};

  ASSERT_EQ(code2[0], 3);
  ASSERT_EQ(code2[1], 2);
  ASSERT_EQ(code2[2], 4);
  ASSERT_EQ(code2[3], 1);
}

TEST(CodeTest, CheckMoveConstructor) {
  Code code1(4);
  code1[0] = 3;
  code1[1] = 2;
  code1[2] = 4;
  code1[3] = 1;

  Code code2{std::move(code1)};

  ASSERT_EQ(code2[0], 3);
  ASSERT_EQ(code2[1], 2);
  ASSERT_EQ(code2[2], 4);
  ASSERT_EQ(code2[3], 1);

  ASSERT_EQ(code1.numPegs(), 0);
}

TEST(CodeTest, CheckAssignment) {
  Code code1(4);
  code1[0] = 3;
  code1[1] = 2;
  code1[2] = 4;
  code1[3] = 1;

  Code code2(0);
  code2 = code1;

  ASSERT_EQ(code2[0], 3);
  ASSERT_EQ(code2[1], 2);
  ASSERT_EQ(code2[2], 4);
  ASSERT_EQ(code2[3], 1);
}

TEST(CodeTest, CheckMoveAssigment) {
  Code code1(4);
  code1[0] = 3;
  code1[1] = 2;
  code1[2] = 4;
  code1[3] = 1;

  Code code2(0);
  code2 = std::move(code1);

  ASSERT_EQ(code2[0], 3);
  ASSERT_EQ(code2[1], 2);
  ASSERT_EQ(code2[2], 4);
  ASSERT_EQ(code2[3], 1);

  ASSERT_EQ(code1.numPegs(), 0);
}

TEST(CodeTest, CheckAccessBounds) {
  Code code(4);

  ASSERT_THROW(code[5], std::out_of_range);
}

TEST(CodeTest, CheckHasValue) {
  Code code(4);
  code[0] = 3;
  code[1] = 2;
  code[2] = 4;
  code[3] = 1;

  EXPECT_TRUE(code.hasValue(3));
  EXPECT_TRUE(code.hasValue(2));
  EXPECT_TRUE(code.hasValue(4));
  EXPECT_TRUE(code.hasValue(1));

  EXPECT_FALSE(code.hasValue(5));
  EXPECT_FALSE(code.hasValue(0));
}

TEST(CodeTest, CheckGuessSizeCheck) {
  Code code(4);
  Code guess(5);

  EXPECT_THROW(code.checkGuess(guess), std::invalid_argument);
}

TEST(CodeTest, CheckGuess) {
  Code code(4);
  code[0] = 3;
  code[1] = 2;
  code[2] = 4;
  code[3] = 1;

  GuessResult result;

  Code guess1(4);
  guess1[0] = 1;
  guess1[1] = 2;
  guess1[2] = 3;
  guess1[3] = 4;
  result = code.checkGuess(guess1);

  EXPECT_EQ(result.correctColorAndPosition, 1);
  EXPECT_EQ(result.correctColor, 3);

  Code guess2(4);
  guess2[0] = 1;
  guess2[1] = 5;
  guess2[2] = 4;
  guess2[3] = 3;

  result = code.checkGuess(guess2);
  EXPECT_EQ(result.correctColorAndPosition, 1);
  EXPECT_EQ(result.correctColor, 2);

  Code guess3(4);
  guess3[0] = 3;
  guess3[1] = 2;
  guess3[2] = 4;
  guess3[3] = 1;
  result = code.checkGuess(guess3);
  EXPECT_EQ(result.correctColorAndPosition, 4);
  EXPECT_EQ(result.correctColor, 0);
}
