/* Mastermind demo.
Copyright (C) 2022  Thomas Liennard

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include "libmastermind/mastermind.hpp"

#include <stdexcept>

namespace libmastermind {
Code::Code(size_t num_pegs) { mPegs.resize(num_pegs); };

Code::Code(const Code& from) = default;
Code::Code(Code&& from) noexcept = default;
Code::~Code() = default;

Code& Code::operator=(const Code& from) = default;
Code& Code::operator=(Code&& from) noexcept = default;

unsigned Code::operator[](size_t index) const {
  // use at to raise an exception instead of having memory corruption if index
  // is invalid
  return mPegs.at(index);
}
unsigned& Code::operator[](size_t index) { return mPegs.at(index); }

size_t Code::numPegs() const noexcept { return mPegs.size(); }

bool Code::hasValue(unsigned value) const noexcept {
  for (auto v : mPegs) {
    if (v == value) return true;
  }
  return false;
}

GuessResult Code::checkGuess(const Code& guess) const {
  if (guess.numPegs() != numPegs())
    throw std::invalid_argument(
        "Code::checkGuess: guess and code must have same size");
  GuessResult result;
  for (size_t i = 0; i < numPegs(); i++) {
    if (mPegs[i] == guess[i]) {
      result.correctColorAndPosition++;
    } else if (hasValue(guess[i])) {
      result.correctColor++;
    }
  }
  return result;
}

Game::Game() : mCode(numPegs()){};
size_t Game::numPegs() const noexcept {
  return 4;
};  // should use a constant, will be dynamic in a later version
size_t Game::maxGuesses() const noexcept { return 12; };

const Code& Game::code() const noexcept { return mCode; };
std::vector<Guess> Game::guesses() const noexcept { return mGuesses; };

bool Game::isFinished() const noexcept {
  return mCodeFound || mGuesses.size() == maxGuesses();
}

bool Game::codeFound() const noexcept { return mCodeFound; };
void Game::startGame(const Code& code) {
  mCode = code;
  mGuesses.clear();
  mCodeFound = false;
}

void Game::tryGuess(const Code& guess) {
  if (isFinished())
    throw std::runtime_error("Game::tryGuess: game already finished");
  auto guess_result = mCode.checkGuess(guess);
  if (guess_result.correctColorAndPosition == numPegs()) mCodeFound = true;
  mGuesses.emplace_back(Guess{guess, guess_result});
}

}  // namespace libmastermind